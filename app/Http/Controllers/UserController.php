<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request['id'];
        if (isset($id) && !empty($id)) {
            $userData = User::where('id','=',$id)->first();
            if (!empty($userData)) {
              $response = array('status' =>'SUCC' ,'msg'=>'Request Success',$userData);
              return json_encode($response);
            }else {
              $response = array('status' =>'ERR' ,'msg'=>'User not found');
              return json_encode($response);
            }
        }else {
          $response = array('status' =>'ERR' ,'msg'=>'please send id in params');
          return json_encode($response);
        }
    }
}
